







$('#btnAceptar').on("click", function () {


    $("#formulario1").validate(
        {
            rules: {
                "rut": {
                    required: true,
                    minlength: 8,                  
                },

                "txtNombre": {
                    required: true,
                    minlength: 4
                },
                "txtEmail": {
                    required: true,
                    email: true
                },
                "txtNacimiento": {
                    required: true,
                    

                },
                "regiones": {
                    required: true,
                },
                 "comunas": {
                    required: true,
                }
            },
            messages: {
                "rut": {
                    required: '<p style="color: red">El campo Rut es requerido</p>',
                    minlength: '<p style="color: red">8 caracteres minimo</p>'
                   

                },
                
                "txtNombre": {
                    required: '<p style="color: red">El campo Nombre es requerido</p>',
                    minlength: "4 Caracters minimo",

                },
                "txtEmail": {
                    required: '<p style="color: red">El campo Email es requerido</p>',
                    email: '<p style="color: red">Ingrese un Email valido. ejemplo@gmail.com</p>'
                },
                "txtNacimiento": {
                    required:'<p style="color: red">El campo Fecha de nacimiento es requerido</p>',
                    
                },
                "regiones": {
                    required:'<p style="color: red">El campo es requerido</p>',
                    
                },
                "comunas": {
                    required:'<p style="color: red">El campo es requerido</p>',
                    
                }
               
            }
        }


        

    );

    var rut = $('#rut').val();
    var fecha = $('#txtNacimiento').val();

    validaRut(rut);
    validarFormatoFecha(fecha)

}


);

function validarFormatoFecha(campo) {
    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
    if ((campo.match(RegExPattern)) && (campo!='')) {
          return true;
    } else {
          return false;
    }
}


function validaRut(rut) {
    var suma = 0;
    var arrRut = rut.split("-");
    var rutSolo = arrRut[0];
    var verif = arrRut[1];
    var continuar = true;
    for (i = 2; continuar; i++) {
        suma += (rutSolo % 10) * i;
        rutSolo = parseInt((rutSolo / 10));
        i = (i == 7) ? 1 : i;
        continuar = (rutSolo == 0) ? false : true;
    }
    resto = suma % 11; dv = 11 - resto;
    if (dv == 10) {
        if (verif.toUpperCase() == 'K') return true;
    } else if (dv == 11 && verif == 0)
        return true;
    else if (dv == verif) return true;
    else alert("RUT incorrecto, ingréselo en el formato 11111111-1");
    return false;

}






























