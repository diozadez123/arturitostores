
from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required

from .models import Cliente, Producto
from .forms import ProductoForm

from .serializers import ProductoSerializer
from rest_framework import generics

# PARA LA API

class API_objects(generics.ListCreateAPIView):
        queryset = Producto.objects.all()
        serializer_class = ProductoSerializer

class API_objects_details(generics.RetrieveUpdateDestroyAPIView):
        queryset = Producto.objects.all()
        serializer_class = ProductoSerializer


# FUNCIONES DEL CRUD 




def editar_producto(request, producto_id):
    
    instancia= Producto.objects.get(idproducto=producto_id)
   
    form=  ProductoForm(instance=instancia)
    
    if request.method=="POST":
       
        form= ProductoForm(request.POST, instance=instancia)
        
        if form.is_valid():
            
            instancia= form.save(commit=False)
            
            instancia.save()
    return render(request, "store/editar_producto.html",{'form':form})   

def borrar_producto(request, producto_id):    
    instacia= Producto.objects.get(idproducto=producto_id)
    instacia.delete()

    return HttpResponse('<script>alert("Eliminado Correctamente."); window.location.href="/listar_productos_full";</script>')
    

def inscripcion_producto(request):
    if request.method == "POST":
        form = ProductoForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.save()
            return redirect('/agregarProducto')
    else:
        form = ProductoForm()
        return render(request, 'store/inscripcion_producto.html',
                      {'form': form})  

def listar_productos_full(request):
    # creamos una colección la cual carga TODOS los registos
    productos = Producto.objects.all()
    # renderizamos la colección en el template
    return render(request,
        "store/listar_productos_full.html", {'productos': productos})


def listar_productos_filtrados(request):

    filtro = request.POST.get()
    
    resultado = Producto.objects.all()

def formulario(request):
    return render(request, "store/registrarse.html", {})
    
def inicio(request):
    return render(request,'store/inicio.html',{'cliente': Cliente.objects.all()})
   
def registrar_cliente(request):
    rut = request.POST.get('rut','')
    contrasena = request.POST.get('contrasenia','')
    nombre = request.POST.get('nombre','')
    email = request.POST.get('email','')
    telefono = request.POST.get('telefono','')
    region = request.POST.get('region','')
    comuna = request.POST.get('comuna','')
    calle = request.POST.get('calle','')

    cliente = Cliente(rut=rut,contrasena=contrasena,nombre=nombre,email=email,telefono=telefono,region=region,comuna=comuna,calle=calle)

    user = User.objects.create_user(rut, email, contrasena)

    user.first_name = nombre
    
    user.save()
    
    Cliente.save(cliente)

    return HttpResponse('<script>alert("Registrado Correctamente."); window.location.href="/";</script>')
    

 
    
def login(request):
    return render(request,'store/login.html') 

def login_iniciar(request):
    usuario = request.POST.get('rut')
    contrasena = request.POST.get('contrasena')
    user = authenticate(request,username=usuario, password=contrasena)

    if user is not None:
        auth_login(request, user)
        return HttpResponse('<script>; window.location.href="/";</script>')
    else:
        return HttpResponse('<script>alert("Rut o contraseña incorrecto, intenta nuevamente..."); window.location.href="/login";</script>')

@login_required(login_url='/login/')
def cerrar_session(request):
    logout(request)
    return HttpResponse('<script> window.location.href="/";</script>')  

def cambiar_contrasenia(request):
    return render(request,'store/cambiar_contrasenia.html') 


def cambio_contrasenia(request):

    inputUsuario = request.POST.get('usuario')

    inputEmail = request.POST.get('email')

    try:

      usuario= User.objects.get(username=inputUsuario)
    except User.DoesNotExist:
      usuario = None

    try:

      email = User.objects.get(email=inputEmail)
    except User.DoesNotExist:
      email = None    

    if usuario is not None:

        if email is not None:
           user= User.objects.get(username=inputUsuario)
           return render(request, "store/nueva_contrasena.html", {'user': user})
        else:
          return HttpResponse('<script>alert("Email aun no registrado, intenta nuevamente..."); window.location.href="/cambiar_contrasenia";</script>')   
    else:
       return HttpResponse('<script>alert("Usuario no registrado, intenta nuevamente..."); window.location.href="/cambiar_contrasenia";</script>')
              

def nueva_contrasena(request,nombre):

    user = User.objects.get(username=nombre)

    contrasena = request.POST.get('contrasena','')

    user.set_password(contrasena) 

    user.save()

    print(user.password)


    return render(request,'store/contacto.html') 

   
def contacto(request):
    return render(request, 'store/contacto.html', {})




