from django.contrib import admin

from .models import Cliente,Producto

class AdminCliente(admin.ModelAdmin):
   list_display = ["rut","contrasena","nombre","email","region"]
   list_filter = ["region"]
   search_fields = ["nombre", "rut","email"]
   ##list_editable = ["rut"]
   class Meta:
       model = Cliente

admin.site.register(Cliente)
admin.site.register(Producto)





