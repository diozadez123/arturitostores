from django.urls import path, include
from . import views

from django.conf.urls import url

urlpatterns = [
    path('api/', views.API_objects.as_view()),
    path('api/<int:pk>/', views.API_objects_details.as_view()),
]


urlpatterns = [  
    #URL de pagina 
    path('', views.inicio),
    path('inicio', views.inicio),
    path('registrarse', views.formulario),
    path('contacto', views.contacto),
    path('login', views.login),

    url(r'^crear_cliente$', views.registrar_cliente,name = "crear_cliente"),
    url(r'^login/iniciar$',views.login_iniciar,name="iniciar"),
    url(r'^cerrarsesion$',views.cerrar_session,name="cerrar_session"),

    #URL del crud
    path('editar_producto/<int:producto_id>', views.editar_producto),
    path('borrar_producto/<int:producto_id>', views.borrar_producto),
    path('agregarProducto',views.inscripcion_producto),
    path('listar_productos_full',views.listar_productos_full),
    path('cambiar_contrasenia',views.cambiar_contrasenia),
    path('cambio_contrasena',views.cambio_contrasenia),
    path('nueva_contrasena/<str:nombre>',views.nueva_contrasena),
    
    
    

   
    
]

