from django.db import models

class Cliente (models.Model):
    rut = models.CharField(max_length=12)
    contrasena = models.CharField(max_length=50)
    nombre = models.CharField(max_length=120)
    email = models.EmailField()   
    telefono = models.IntegerField()
    region = models.CharField(max_length=120, blank=True, null=True)
    comuna = models.CharField(max_length=120, blank=True, null=True)
    calle = models.CharField(max_length=120, blank=True, null=True)
    

    def __str__(self):
        return "cliente"

class Producto (models.Model):
    idproducto = models.CharField(max_length=10)
    nombre = models.CharField(max_length=50)
    stock = models.IntegerField()
    categoria = models.CharField(max_length=50)
    precio = models.IntegerField()


     
     
    def __str__(self):
        return self.nombre        
