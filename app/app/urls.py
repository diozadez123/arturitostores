"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
  	  	path('admin/', admin.site.urls),
    	path('', include('arturitostore.urls')),
        path('inicio', include('arturitostore.urls')),
        path('registrarse', include('arturitostore.urls')),
        path('contacto', include('arturitostore.urls')),
        path('login', include('arturitostore.urls')),
        #URL DEL CRUD
        path('editar_producto/<int:producto_id>', include('arturitostore.urls')),
        path('borrar_producto/<int:producto_id>', include('arturitostore.urls')),
        path('agregarProducto', include('arturitostore.urls')),
        path('listar_productos_full', include('arturitostore.urls')),
        path('nueva_contrasena', include('arturitostore.urls')),
        
        
]

